use std::fs;
use std::collections::HashMap;
use std::collections::HashSet;

use regex::Regex;


pub fn solve() {
	let data: Vec<HashMap<String, String>> = task_data();
	println!("There is {:?}", data.len());
	println!("Day 4, task 1: {:?}", task1(&data));
	println!("Day 4, task 2: {:?}", task2(&data));
}

fn task_data() -> Vec<HashMap<String, String>> {
	fs::read_to_string("data/day4.txt")
        .expect("file not found!")
        .split("\n\n")
        .map(parse_passport)
        .collect()
}

fn parse_passport(passport_data: &str) -> HashMap<String, String> {
	passport_data.replace("\n", " ")
		.split(" ")
		.map(parse_pair)
		.collect::<HashMap<String, String>>()

}

fn parse_pair(pair: &str) -> (String, String) {
	let tup = pair.split_at(pair.find(':').expect("Invalid pair!"));
	(String::from(tup.0), String::from(&(tup.1[1..])))
}


fn task1(passports: &Vec<HashMap<String, String>>) -> usize {

	passports.iter()
		.filter(|x| has_keys(x))
		.count()
}



fn task2(passports: &Vec<HashMap<String, String>>) -> usize {
	passports.iter()
		.filter(|x| has_keys(x))
		.filter(|x| is_valid_byr(x.get("byr").unwrap()))
		.filter(|x| is_valid_iyr(x.get("iyr").unwrap()))
		.filter(|x| is_valid_eyr(x.get("eyr").unwrap()))
		.filter(|x| is_valid_hgt(x.get("hgt").unwrap()))
		.filter(|x| is_valid_hcl(x.get("hcl").unwrap()))
		.filter(|x| is_valid_ecl(x.get("ecl").unwrap()))
		.filter(|x| is_valid_pid(x.get("pid").unwrap()))
		.count()
}

fn has_keys(passport: &HashMap<String, String>) -> bool {
	let required_keys: HashSet<String> = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"].iter().map(|x| String::from(*x)).collect();
	passport.keys().cloned().collect::<HashSet<_>>().is_superset(&required_keys)
}

fn is_valid_byr(byr: &str) -> bool {
	num_in_range(byr, 1920, 2002)
}
fn is_valid_iyr(iyr: &str) -> bool {
	num_in_range(iyr, 2010, 2020)
}
fn is_valid_eyr(eyr: &str) -> bool {
	num_in_range(eyr, 2020, 2030)
}

fn is_valid_hgt(hgt: &str) -> bool {
	let x = hgt.len()-2;
	match &hgt[x..] {
		"cm" => num_in_range(&hgt[..x], 150, 193),
		"in" => num_in_range(&hgt[..x], 59, 76),
		_ => false
	}

}
fn is_valid_hcl(hcl: &str) -> bool {
	match_pattern(hcl, "^#([a-f]|\\d){6}$")
}
fn is_valid_ecl(ecl: &str) -> bool {
	match_pattern(ecl, "^(amb|blu|brn|gry|grn|hzl|oth)$")
}
fn is_valid_pid(pid: &str) -> bool {
	match_pattern(pid, "^\\d{9}$")
}

fn num_in_range(num: &str, min: i32, max: i32) -> bool {
	match num.parse::<i32>() {
		Ok(x) => (min <= x && x <= max),
		Err(_) => false
	}
}

fn match_pattern(val: &str, pattern: &str) -> bool {
	Regex::new(pattern).unwrap().is_match(val)
}