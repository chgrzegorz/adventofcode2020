use std::fs;


pub fn solve() {
	let data: Vec<String> = task_data();
	println!("Day 5, task 1: {:?}", task1(&data));
	println!("Day 5, task 2: {:?}", task2(&data));
}

fn task_data() -> Vec<String> {
	fs::read_to_string("data/day5.txt")
        .expect("file not found!")
        .lines()
        .map(|x| x.parse::<String>().expect("Invalid line!"))
        .collect()
}

fn task1(boarders: &Vec<String>) -> i32 {
	boarders.iter().map(find_row)
		.zip(boarders.iter().map(find_seat))
		.map( |(r, s)| r*8 + s)
		.max().unwrap()
}

fn task2(boarders: &Vec<String>) -> i32 {
	let mut all = boarders.iter().map(find_row)
		.zip(boarders.iter().map(find_seat))
		.map( |(r, s)| r*8 + s)
		.collect::<Vec<i32>>();

	all.sort();

	all.iter().cloned()
		.zip(all[0]..)
		.find_map(|(x, y)| if x != y {Some(y)} else {None}).unwrap()
}

fn find_row(boarder: &String) -> i32 {
	boarder.chars()
		.take(7)
		.fold((0, 127), |acc, key| match key {
			'F' => left_half(acc),
			_ => right_falt(acc)
		}).0
	
}

fn find_seat(boarder: &String) -> i32 {
	boarder.chars()
		.skip(7)
		.fold((0, 7), |acc, key| match key {
			'L' => left_half(acc),
			_ => right_falt(acc)
		}).1
	
}

fn left_half(acc: (i32, i32)) -> (i32, i32) {
	let mid =  acc.0 + (acc.1 - acc.0) / 2;
	(acc.0, mid)
}


fn right_falt(acc: (i32, i32)) -> (i32, i32) {
	let d = acc.1 - acc.0;
	
	(acc.0 + (d / 2) + (d % 2), acc.1)
}

