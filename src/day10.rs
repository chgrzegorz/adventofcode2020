use std::fs;

pub fn solve() {
    let data = task_data();
    println!("Day 10, task 1: {:?}", task1(&data));
    println!("Day 10, task 2: {:?}", task2(&data));
}

fn task_data() -> Vec<usize> {
    fs::read_to_string("data/day10.txt")
        .expect("file not found!")
        .lines()
        .map(|x| x.parse::<usize>().expect("Invalid line!"))
        .collect::<Vec<usize>>()
}

fn task1(adapters: &Vec<usize>) -> usize {
    let mut sorted_adapters: Vec<usize> = adapters.clone();
    sorted_adapters.sort_by(|a, b| a.cmp(b));
    count_diffs(&sorted_adapters)
}

fn task2(adapters: &Vec<usize>) -> usize {
    let mut sorted_adapters: Vec<usize> = adapters.clone();
    sorted_adapters.sort_by(|a, b| a.cmp(b));

    // stolen code:
    2 * sorted_adapters
            .windows(2)
            .collect::<Vec<_>>()
            .split(|n| n[1] - n[0] == 3)
            .map(|n| match n.len() {
                4 => 7,
                3 => 4,
                2 => 2,
                _ => 1,
            })
            .product::<usize>()
    
}

fn count_diffs(adapters: &Vec<usize>) -> usize {
	let diffs = adapters.iter()
		.fold((&0, 0, 0, 0), |acc, x| {
			match x - acc.0 {
				3 => (x, acc.1, acc.2, acc.3 + 1),
				2 => (x, acc.1, acc.2 + 1, acc.3),
				1 => (x, acc.1 + 1, acc.2, acc.3),
				_ => (x, acc.1, acc.2, acc.3),
			}
		});
	diffs.1 * (diffs.3 + 1)
}
