use std::fs;

const MAX_COL: usize = 91;
const MAX_ROW: usize = 90;

const FLOOR: char = '.';
const EMPTY: char = 'L';
const OCCUP: char = '#';

pub fn solve() {
    println!("Day 11, task 1: {:?}", task1(task_data()));
    println!("Day 11, task 2: {:?}", task2(task_data()));
}

fn task_data() -> Vec<Vec<char>> {
    fs::read_to_string("data/day11.txt")
        .expect("file not found!")
        .lines()
        .map(|line| line.chars().collect::<Vec<char>>())
        .collect::<Vec<Vec<char>>>()
}

fn task1(board: Vec<Vec<char>>) -> usize {
    stabilize1(board)
        .iter()
        .flatten()
        .filter(|x| **x == OCCUP)
        .count()
}

fn stabilize1(board: Vec<Vec<char>>) -> Vec<Vec<char>> {
    let new_board = run_epoch1(&board);
    if new_board.iter().flatten().eq(board.iter().flatten()) {
        new_board
    } else {
        stabilize1(new_board)
    }
}

fn run_epoch1(board: &Vec<Vec<char>>) -> Vec<Vec<char>> {
    (0..90)
        .map(|row| {
            (0..91)
                .map(|col| {
                    let seat = board[row][col];
                    if seat == FLOOR {
                        seat
                    } else {
                        match (seat, get_occupied1(board, row, col)) {
                            (EMPTY, 0) => OCCUP,
                            (OCCUP, x) if x > 3 => EMPTY,
                            _ => seat,
                        }
                    }
                })
                .collect::<Vec<char>>()
        })
        .collect::<Vec<Vec<char>>>()
}

fn get_occupied1(board: &Vec<Vec<char>>, row: usize, col: usize) -> usize {
    let indices = match (row, col) {
        (0, 0) => vec![(0, 1), (1, 1), (1, 0)],
        (0, 90) => vec![(1, 90), (1, 89), (0, 89)],
        (89, 90) => vec![(89, 89), (88, 89), (88, 90)],
        (89, 0) => vec![(88, 1), (89, 1), (88, 0)],
        (0, c) => vec![(0, c + 1), (1, c + 1), (1, c), (1, c - 1), (0, c - 1)],
        (r, 90) => vec![(r + 1, 90), (r + 1, 89), (r, 89), (r - 1, 89), (r - 1, 90)],
        (89, c) => vec![(88, c + 1), (89, c + 1), (89, c - 1), (88, c - 1), (88, c)],
        (r, 0) => vec![(r, 1), (r + 1, 1), (r + 1, 0), (r - 1, 0), (r - 1, 1)],
        (r, c) => vec![
            (r - 1, c + 1),
            (r, c + 1),
            (r + 1, c + 1),
            (r + 1, c),
            (r + 1, c - 1),
            (r, c - 1),
            (r - 1, c - 1),
            (r - 1, c),
        ],
    };

    indices
        .iter()
        .map(|(r, c)| board[*r][*c])
        .filter(|x| *x == OCCUP)
        .count()
}

fn task2(board: Vec<Vec<char>>) -> usize {
    stabilize2(board)
        .iter()
        .flatten()
        .filter(|x| **x == OCCUP)
        .count()
}

fn stabilize2(board: Vec<Vec<char>>) -> Vec<Vec<char>> {
    let new_board = run_epoch2(&board);
    if new_board.iter().flatten().eq(board.iter().flatten()) {
        new_board
    } else {
        stabilize2(new_board)
    }
}

fn run_epoch2(board: &Vec<Vec<char>>) -> Vec<Vec<char>> {
    (0..MAX_ROW)
        .map(|row| {
            (0..MAX_COL)
                .map(|col| {
                    let seat = board[row][col];
                    if seat == FLOOR {
                        seat
                    } else {
                        match (seat, get_occupied2(board, row, col)) {
                            (EMPTY, 0) => OCCUP,
                            (OCCUP, x) if x > 4 => EMPTY,
                            _ => seat,
                        }
                    }
                })
                .collect::<Vec<char>>()
        })
        .collect::<Vec<Vec<char>>>()
}

fn get_occupied2(board: &Vec<Vec<char>>, row: usize, col: usize) -> usize {
    [
        L(board, row, col),
        R(board, row, col),
        U(board, row, col),
        D(board, row, col),
        UR(board, row, col),
        UL(board, row, col),
        DR(board, row, col),
        DL(board, row, col),
    ]
    .iter()
    .filter(|x| match x {
        Some(OCCUP) => true,
        _ => false,
    })
    .count()
}

fn L(board: &Vec<Vec<char>>, row: usize, col: usize) -> Option<char> {
    (0..col)
        .rev()
        .map(|c| board[row][c])
        .skip_while(|x| *x == '.')
        .next()
}

fn R(board: &Vec<Vec<char>>, row: usize, col: usize) -> Option<char> {
    ((col + 1)..MAX_COL)
        .map(|c| board[row][c])
        .skip_while(|x| *x == FLOOR)
        .next()
}

fn U(board: &Vec<Vec<char>>, row: usize, col: usize) -> Option<char> {
    (0..row)
        .rev()
        .map(|r| board[r][col])
        .skip_while(|x| *x == '.')
        .next()
}

fn D(board: &Vec<Vec<char>>, row: usize, col: usize) -> Option<char> {
    ((row + 1)..MAX_ROW)
        .map(|r| board[r][col])
        .skip_while(|x| *x == '.')
        .next()
}

fn UR(board: &Vec<Vec<char>>, row: usize, col: usize) -> Option<char> {
    (0..row)
        .rev()
        .zip((col + 1)..MAX_COL)
        .map(|(r, c)| board[r][c])
        .skip_while(|x| *x == '.')
        .next()
}

fn UL(board: &Vec<Vec<char>>, row: usize, col: usize) -> Option<char> {
    (0..row)
        .rev()
        .zip((0..col).rev())
        .map(|(r, c)| board[r][c])
        .skip_while(|x| *x == '.')
        .next()
}

fn DR(board: &Vec<Vec<char>>, row: usize, col: usize) -> Option<char> {
    ((row + 1)..MAX_ROW)
        .zip((col + 1)..MAX_COL)
        .map(|(r, c)| board[r][c])
        .skip_while(|x| *x == '.')
        .next()
}

fn DL(board: &Vec<Vec<char>>, row: usize, col: usize) -> Option<char> {
    ((row + 1)..MAX_ROW)
        .zip((0..col).rev())
        .map(|(r, c)| board[r][c])
        .skip_while(|x| *x == '.')
        .next()
}
