use std::collections::HashMap;
use std::fs;

use itertools::Itertools;

pub fn solve() {
    let data = fs::read_to_string("data/day14.txt").expect("file not found!");
    println!("Day 14, task 1: {:?}", task1(&data));
    println!("Day 14, task 2: {:?}", task2(&data));
}

fn task1(program: &str) -> i64 {
    // let mut memory: HashMap<String, String> = HashMap::new();
    program.lines()
        .fold((HashMap::new(), String::new()), |(mut mem, mask), line| {
            if line.starts_with("mask") {
                (mem, line.chars().skip(7).collect::<String>())
            } else {
                let addr = line.chars().skip(4).take_while(|x| *x != ']').collect::<String>();
                let val_dec = line.chars().skip_while(|x| *x != '=').skip(2).collect::<String>().parse::<i64>().unwrap();
                let bin = format!("{:036b}", val_dec);
                mem.insert(addr, apply_mask(&bin, &mask));
                (mem, mask)
            }
        }).0.iter()
        .map(|(_, value)| bin_to_dec(value))
        .sum()
}

fn bin_to_dec(bin: &str) -> i64 {
    let base: i64 = 2;
    bin.chars()
        .rev()
        .enumerate()
        .fold(0, |acc, (i, x)| {
            match x {
                '1' => acc + base.pow(i as u32),
                _ => acc,
            }
        } )
}

fn apply_mask(bin: &str, mask: &str) -> String {
    bin.chars()
        .zip(mask.chars())
        .map(|(b, m)| match m {
            'X' => b,
            _ => m,
        }).collect()
}

fn task2(program: &str) -> i64 {
    program.lines()
        .fold((HashMap::new(), String::new()), |(mut mem, mask), line| {
            if line.starts_with("mask") {
                (mem, line.chars().skip(7).collect::<String>())
            } else {
                let val = line.chars().skip_while(|x| *x != '=').skip(2).collect::<String>().parse::<i64>().unwrap();
                let dec_addr = line.chars().skip(4).take_while(|x| *x != ']').collect::<String>().parse::<i64>().unwrap();
                let bin_addr = format!("{:036b}", dec_addr);
                let addresses = memory_address_decoder(&bin_addr, &mask);
                for addr in addresses.iter().cloned() {
                    let dec_addr = bin_to_dec(&addr);
                    mem.insert(dec_addr, val);
                }
                (mem, mask)
            }
        }).0.iter()
        .map(|(_, value)| value)
        .sum()
}



fn memory_address_decoder(addr: &str, mask: &str) -> Vec<String> {
    let base: usize = 2;
    let mut k = 0;
    let floating_addr = addr.chars()
        .zip(mask.chars())
        .map(|(a, m)| match m {
            '0' => a,
            '1' => '1',
            x => {k += 1; x}
        }).collect::<String>();

    (0..2usize.pow(k))
        .map(|x| format!("{:036b}", x).chars().skip((36-k) as usize).collect())
        .map(|bits| resolve_floating(&floating_addr, bits))
        .collect()
}

fn resolve_floating(floating_addr: &str, bits: String) -> String {
    let mut biter = bits.chars();
    floating_addr.chars()
        .map(|a| match a {
            'X' => biter.next().unwrap(),
            x => x,
        }).collect()
}