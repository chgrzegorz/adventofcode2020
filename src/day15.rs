use std::collections::HashMap;


pub fn solve() {
    let starting_numbers =  vec![8,0,17,4,1,12];
    // let starting_numbers =  vec![0, 3, 6];
    println!("Day 15, task 1: {:?}", task1(&starting_numbers));
    println!("Day 15, task 2: {:?}", task2(&starting_numbers));
}

fn task1(starting_numbers: &Vec<i64>) -> i64 {
    let mut history: HashMap<i64, i64> = HashMap::new();
    for (i, n) in starting_numbers.iter().enumerate() {
        println!("{:?}, {:?}", i+1, n);
        history.insert(*n, i as i64);
    }
    let start_len = starting_numbers.len() as i64;
    let mut next: i64 = 0;
    for turn in start_len..30000000-1 {
        match history.insert(next, turn as i64) {
            None => next = 0,
            Some(x) => next = turn as i64 - x
        };
    }
    next
}


fn task2(starting_numbers: &Vec<i64>) -> i64 {
    -1
}
