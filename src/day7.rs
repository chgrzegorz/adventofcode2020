use std::collections::HashSet;
use std::collections::VecDeque;
use std::fs;

pub fn solve() {
    let data = task_data();
    println!("Day 7, task 1: {:?}", task1(&data));
    println!("Day 7, task 2: {:?}", task2(&data));
}
#[derive(Clone, Debug)]
struct Edge {
    from: String,
    to: String,
    count: usize,
}

fn task_data() -> Vec<Edge> {
    fs::read_to_string("data/day7.txt")
        .expect("file not found!")
        .lines()
        .map(|x| x.replace(" bags contain ", ":"))
        .map(parse_rule)
        .flatten()
        .collect::<Vec<Edge>>()
}

fn task1(edges: &Vec<Edge>) -> usize {
    let looking_for = "shiny gold";
    let to_check = vec![String::from(looking_for)];
    let result: Vec<String> = search(Vec::new(), to_check, edges);
    result.iter().collect::<HashSet<&String>>().iter().count() - 1
}
fn task2(edges: &Vec<Edge>) -> usize {
    how_many("shiny gold", edges) - 1
}

fn parse_rule(rule: String) -> Vec<Edge> {
    match rule.find("no other bags") {
        Some(_) => return Vec::new(),
        None => (),
    }
    let sepi = rule.find(":").unwrap();
    let from = match rule.get(..sepi) {
        Some(x) => x,
        None => panic!(rule),
    };
    rule.get(sepi + 1..)
        .unwrap()
        .split(",")
        .map(|x| make_edge(from, x))
        .collect::<Vec<Edge>>()
}

fn make_edge(from: &str, dest: &str) -> Edge {
    let mut words = dest.trim().split(" ");
    Edge {
        from: String::from(from),
        count: words.next().unwrap().parse::<usize>().unwrap(),
        to: format!("{} {}", words.next().unwrap(), words.next().unwrap()),
    }
}

fn search(found: Vec<String>, to_check: Vec<String>, bag_rules: &Vec<Edge>) -> Vec<String> {
    if to_check.is_empty() {
        return found;
    }

    let step: Vec<String> = to_check
        .iter()
        .map(|en| to_this(en, bag_rules))
        .flatten()
        .collect::<Vec<String>>();

    let new_found: Vec<String> = found.iter().chain(to_check.iter()).cloned().collect();
    search(new_found, step, bag_rules)
}

fn to_this(looking_for: &str, bag_rules: &Vec<Edge>) -> Vec<String> {
    bag_rules
        .iter()
        .filter(|e| e.to == looking_for)
        .map(|e| e.from.clone())
        .collect()
}

fn how_many(bag: &str, bag_rules: &Vec<Edge>) -> usize {
    let contains: Vec<&Edge> = bag_rules.iter().filter(|e| e.from == bag).collect();

    match contains.len() {
        0 => 1,
        _ => {
            let s: usize = contains
                .iter()
                .map(|e| e.count * how_many(&e.to, bag_rules))
                .sum();
            s + 1
        }
    }
}
