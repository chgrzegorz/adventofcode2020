use std::fs;
use regex::Regex;

pub fn solve() {
	let data = task_data();
	println!("Day 2, task 1: {:?}", task1(&data));
	println!("Day 2, task 2: {:?}", task2(&data));
}


struct Rule {
	letter: char,
	min: usize,
	max: usize
}


fn task_data() -> Vec<(Rule, String)> {
	fs::read_to_string("data/day2.txt")
        .expect("file not found!")
        .lines()
        .map(split_line)
        .collect()
}

fn split_line(line: &str) -> (Rule, String) {
	let p1 = line.find('-').expect("Malformed line!");
	let p2 = line.find(' ').expect("Malformed line!");
	let p3 = line.find(':').expect("Malformed line!");

	(
		Rule {
			min: line[0..p1].parse::<usize>().expect("Incorrect min!"),
			max: line[p1+1..p2].parse::<usize>().expect("Incorrect max!"),
			letter: line.chars().nth(p2+1).unwrap()
		},
		String::from(&line[p3+2..])
	)
}

fn task1(data: &Vec<(Rule, String)>) -> usize {
	data.iter()
		.map(|el| check_rule1(&el.0, &el.1))
		.sum()

}

fn task2(data: &Vec<(Rule, String)>) -> usize {
	data.iter()
		.map(|el| check_rule2(&el.0, &el.1))
		.sum()
}

fn check_rule1(rule: &Rule, password: &str) -> usize {
	let pattern = format!("^([^{letter}]*{letter}{{1}}[^{letter}]*){{{min},{max}}}$", min=rule.min, max=rule.max, letter=rule.letter);
	let re = Regex::new(&pattern).unwrap();
	let result = re.is_match(&password);
	result as usize

}

fn check_rule2(rule: &Rule, password: &str) -> usize {
	let first: bool = nth_eq(password, rule.min, rule.letter);
	let second: bool = nth_eq(password, rule.max, rule.letter);
	(first ^ second) as usize

}

fn nth_eq(text: &str, index: usize, letter: char) -> bool {
	match text.chars().nth(index - 1) {
		Some(v) => v == letter,
		None => false 
	}
}