use std::collections::VecDeque;
use std::fs;

pub fn solve() {
    let data = task_data();
    println!("Day 9, task 1: {:?}", task1(&data));
    println!("Day 9, task 2: {:?}", task2(&data));
}

fn task_data() -> Vec<usize> {
    fs::read_to_string("data/day9.txt")
        .expect("file not found!")
        .lines()
        .map(|x| x.parse::<usize>().expect("Invalid line!"))
        .collect::<Vec<usize>>()
}

fn task1(code: &Vec<usize>) -> usize {
    let preamble = code.iter().take(25).cloned().collect::<VecDeque<usize>>();
    let mut dec = Decoder::new(preamble);
    for x in code.iter().skip(25) {
        if !dec.push(*x) {
            return *x;
        }
    }
    1
}

fn task2(code: &Vec<usize>) -> usize {
    let res = crack_the_code(code, task1(code));
    let min = res.iter().min().unwrap();
    let max = res.iter().max().unwrap();
    min + max
}

#[derive(Debug)]
struct Decoder {
    preamble: VecDeque<usize>,
    list: Vec<usize>,
}

impl Decoder {
    pub fn new(preamble: VecDeque<usize>) -> Self {
        Self {
            preamble: preamble.iter().cloned().collect(),
            list: preamble.iter().cloned().collect()
        }
    }

    pub fn push(&mut self, num: usize) -> bool {
        if self.check_num(num) {
            self.preamble.pop_front();
            self.preamble.push_back(num);
            true
        } else {
            false
        }
    }

    fn check_num(&self, num: usize) -> bool {
        // I've tried to find some smart solution but implementation took too long
        for x in self.preamble.iter() {
            if x < &num {
                let y = num - x;
                match self.preamble.iter().find(|&&z| z == y) {
                    Some(_) => return true,
                    None => (),
                }
            }
        }
        false
    }
}


fn crack_the_code(code: &Vec<usize>, num: usize) -> VecDeque<usize> {
    let window: VecDeque<usize> = VecDeque::new();
    fn folder(acc: (VecDeque<usize>, isize), item: &usize) -> (VecDeque<usize>, isize) {
        if acc.1 == 0 { return acc; }

        let mut window = acc.0.clone();
        let mut new_sum = acc.1 - *item as isize;
        window.push_back(*item);
        while new_sum < 0 {
            new_sum += window.pop_front().unwrap() as isize;
        }
        
        (window, new_sum)
    }

    code.iter().fold((window, num as isize), folder).0
}
