use std::fs;

pub fn solve() {
    let data = fs::read_to_string("data/day13.txt").expect("file not found!");
    println!("Day 13, task 1: {:?}", task1(&data));
    println!("Day 13, task 2: {:?}", task2(&data));
}

fn task1(notes: &str) -> i32 {
    let mut notes_lines = notes.lines();
    let estim: i32 = notes_lines.next().unwrap().parse::<i32>().unwrap();
    let busses: String = String::from(notes_lines.next().unwrap());
    let result = busses
        .split(',')
        .filter(|x| *x != "x")
        .map(|id| id.parse::<i32>().unwrap())
        .map(|id| (id, id - estim % id))
        .min_by_key(|x| x.1)
        .unwrap();
    result.0 * result.1
}

fn task2(notes: &str) -> i64 {
    let mut notes_lines = notes.lines();
    notes_lines.next();
    let mut busses_iter = notes
        .lines()
        .nth(1)
        .unwrap()
        .split(',')
        .enumerate()
        .filter(|(_, id)| *id != "x")
        .map(|(i, id)| (i as i64, id.parse::<i64>().unwrap()));

    let first_bus: i64 = busses_iter.next().unwrap().1;
    let other: Vec<(i64, i64)> = busses_iter.collect();

    (100000000000000..)
        .step_by(first_bus as usize)
        .find(|t| other.iter().all(|(i, id)| (t + i) % id == 0))
        .unwrap()
}
