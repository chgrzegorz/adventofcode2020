use std::collections::HashMap;
use std::collections::HashSet;
use std::fs;

pub fn solve() {
    let data = task_data();
    println!("Day 16, task 1: {:?}", task1(&data));
    println!("Day 16, task 2: {:?}", task2(&data));
}

#[derive(Debug, Clone)]
struct Rule {
    name: String,
    ranges: Vec<(i64, i64)>,
}

#[derive(Debug)]
struct PuzzleInput {
    rules: Vec<Rule>,
    my: Vec<i64>,
    nearby: Vec<Vec<i64>>,
}

fn task_data() -> PuzzleInput {
    let puzzle_txt = fs::read_to_string("data/day16.txt").expect("file not found!");
    let mut iter = puzzle_txt.trim().split("your ticket:\n");
    let rules: Vec<Rule> = iter
        .nth(0)
        .unwrap()
        .trim()
        .lines()
        .map(parse_rule)
        .collect();
    let mut iter = iter.nth(0).unwrap().lines();
    let my: Vec<i64> = parse_ticket(iter.nth(0).unwrap());

    let nearby: Vec<Vec<i64>> = iter.skip(2).map(parse_ticket).collect();

    PuzzleInput { rules, my, nearby }
}

fn parse_rule(line: &str) -> Rule {
    let name = line.split(':').nth(0).unwrap().to_string();
    let ranges: Vec<(i64, i64)> = line
        .split(": ")
        .nth(1)
        .unwrap()
        .split(" or ")
        .map(|r| {
            let mut iter = r.split('-').map(|x| x.parse::<i64>().unwrap());
            (iter.next().unwrap(), iter.next().unwrap())
        })
        .collect();
    Rule { name, ranges }
}
fn parse_ticket(line: &str) -> Vec<i64> {
    line.split(',').map(|x| x.parse::<i64>().unwrap()).collect()
}

fn task1(puzzle: &PuzzleInput) -> i64 {
    // It's not optimal, but shouldn't be a problem there
    // Better solution would be to create a sum of ranges
    let ranges: Vec<(i64, i64)> = puzzle
        .rules
        .iter()
        .map(|r| r.ranges.clone())
        .flatten()
        .collect();

    puzzle.nearby.iter().map(|t| sum_invalid(t, &ranges)).sum()
}

fn sum_invalid(ticket: &Vec<i64>, ranges: &Vec<(i64, i64)>) -> i64 {
    ticket
        .iter()
        .filter_map(
            |x| match ranges.iter().find(|range| range.0 < *x && *x < range.1) {
                None => Some(x),
                Some(_) => None,
            },
        )
        .sum()
}

fn task2(puzzle: &PuzzleInput) -> i64 {
    let ranges: Vec<(i64, i64)> = puzzle
        .rules
        .iter()
        .map(|r| r.ranges.clone())
        .flatten()
        .collect();

    let valid_tickets: Vec<Vec<i64>> = puzzle
        .nearby
        .iter()
        .map(|t| (t, sum_invalid(t, &ranges)))
        .filter_map(|(t, x)| match x {
            0 => Some(t),
            _ => None,
        })
        .chain([puzzle.my.clone()].iter())
        .cloned()
        .collect();

    let rules_by_name: HashMap<String, Rule> = puzzle
        .rules
        .iter()
        .map(|r| (r.name.clone(), r.clone()))
        .collect();

    let len = puzzle.my.len();
    let possible: HashMap<String, HashSet<usize>> = puzzle
        .rules
        .iter()
        .map(|rule| (rule.name.clone(), (0..len).collect()))
        .collect();

    let mut filter_result: Vec<(String, HashSet<usize>)> = valid_tickets
        .iter()
        .fold(possible, |acc, ticket| {
            apply_ticket(&rules_by_name, acc, ticket)
        })
        .iter()
        .map(|(x, y)| (x.clone(), y.clone()))
        .collect();

    filter_result.sort_by_key(|(_, y)| y.len());

    filter_result
        .iter()
        .fold((HashMap::new(), HashSet::new()), |(mut mapping, mut seen), fr| {
            let new = fr.1.iter().find(|x| seen.insert(**x)).unwrap();
            mapping.insert(fr.0.clone(), *new);
            (mapping, seen)
        })
        .0
        .iter()
        .filter(|(rn, _)| rn.starts_with("departure"))
        .map(|(_, &i)| puzzle.my[i])
        .product()
}

fn apply_ticket(
    rules_by_name: &HashMap<String, Rule>,
    possible: HashMap<String, HashSet<usize>>,
    ticket: &Vec<i64>,
) -> HashMap<String, HashSet<usize>> {
    possible
        .iter()
        .map(|(name, possibilities)| {
            let rule = rules_by_name.get(name).unwrap();
            (name.clone(), reduce(rule, possibilities, ticket))
        })
        .collect()
}

fn reduce(rule: &Rule, possibilities: &HashSet<usize>, ticket: &Vec<i64>) -> HashSet<usize> {
    possibilities
        .iter()
        .filter(|i| fits_into(ticket[**i], rule))
        .cloned()
        .collect()
}

fn fits_into(x: i64, rule: &Rule) -> bool {
    rule.ranges.iter().any(|r| r.0 <= x && x <= r.1)
}
