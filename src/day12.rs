use std::fs;

pub fn solve() {
    let data = task_data();
    println!("Day 12, task 1: {:?}", task1(&data));
    println!("Day 12, task 2: {:?}", task2(&data));
}

fn task_data() -> Vec<(char, isize)> {
    fs::read_to_string("data/day12.txt")
        .expect("file not found!")
        .lines()
        .map(parse_instruction)
        .collect::<Vec<(char, isize)>>()
}

fn parse_instruction(instruction: &str) -> (char, isize) {
    let mut it = instruction.chars();
    (
        it.next().unwrap(),
        it.collect::<String>().parse::<isize>().unwrap(),
    )
}

#[derive(Debug)]
struct Position {
    facing: isize,
    x: isize,
    y: isize,
}

fn task1(instructions: &Vec<(char, isize)>) -> isize {
    let final_position: Position = instructions.iter().fold(
        Position {
            facing: 0,
            x: 0,
            y: 0,
        },
        apply_move,
    );
    final_position.x.abs() + final_position.y.abs()
}

#[derive(Debug)]
struct Waypoint {
    x: isize,
    y: isize,
}

#[derive(Debug)]
struct Ship {
    x: isize,
    y: isize,
    w: Waypoint,
}

fn task2(instructions: &Vec<(char, isize)>) -> isize {
    let destination: Ship = instructions.iter().fold(
        Ship {
            x: 0,
            y: 0,
            w: Waypoint { x: 10, y: 1 },
        },
        |ship, movement| match movement {
            ('F', times) => Ship {
                x: ship.x + ship.w.x * times,
                y: ship.y + ship.w.y * times,
                w: ship.w,
            },
            ('L', degrees) => Ship {
                x: ship.x,
                y: ship.y,
                w: rotate_waypoint(ship.w, -degrees),
            },
            ('R', degrees) => Ship {
                x: ship.x,
                y: ship.y,
                w: rotate_waypoint(ship.w, *degrees),
            },
            ('E', x) => Ship {
                x: ship.x,
                y: ship.y,
                w: Waypoint {
                    x: ship.w.x + x,
                    y: ship.w.y,
                },
            },
            ('W', x) => Ship {
                x: ship.x,
                y: ship.y,
                w: Waypoint {
                    x: ship.w.x - x,
                    y: ship.w.y,
                },
            },
            ('N', y) => Ship {
                x: ship.x,
                y: ship.y,
                w: Waypoint {
                    x: ship.w.x,
                    y: ship.w.y + y,
                },
            },
            ('S', y) => Ship {
                x: ship.x,
                y: ship.y,
                w: Waypoint {
                    x: ship.w.x,
                    y: ship.w.y - y,
                },
            },
            _ => panic!("Incorrect movement!"),
        },
    );

    destination.x.abs() + destination.y.abs()
}

fn apply_move(p: Position, movement: &(char, isize)) -> Position {
    match (movement, p.facing) {
        (('L', x), _) => apply_move(p, &('R', 360 - x)),
        (('R', x), _) => Position {
            facing: (p.facing + x / 90) % 4,
            x: p.x,
            y: p.y,
        },
        (('E', x), _) | (('F', x), 0) => Position {
            facing: p.facing,
            x: p.x + x,
            y: p.y,
        },
        (('W', x), _) | (('F', x), 2) => Position {
            facing: p.facing,
            x: p.x - x,
            y: p.y,
        },
        (('N', y), _) | (('F', y), 3) => Position {
            facing: p.facing,
            x: p.x,
            y: p.y + y,
        },
        (('S', y), _) | (('F', y), 1) => Position {
            facing: p.facing,
            x: p.x,
            y: p.y - y,
        },
        _ => panic!("Incorrect movement!"),
    }
}

fn rotate_waypoint(w: Waypoint, degrees: isize) -> Waypoint {
    let sign: isize = degrees / degrees.abs();
    let new_w = Waypoint {
        x: sign * w.y,
        y: -1 * sign * w.x,
    };
    match degrees.abs() {
        90 => new_w,
        a => rotate_waypoint(new_w, sign * (a - 90)),
    }
}
