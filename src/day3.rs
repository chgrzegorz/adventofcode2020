use std::fs;


pub fn solve() {
	let data: Vec<String> = task_data();
	println!("Day 3, task 1: {:?}", task1(&data));
	println!("Day 3, task 2: {:?}", task2(&data));
}

fn task_data() -> Vec<String> {
	fs::read_to_string("data/day3.txt")
        .expect("file not found!")
        .lines()
        .map(|x| x.parse::<String>().expect("Invalid line!"))
        .collect()
}

fn task1(terrain: &Vec<String>) -> i64 {
	ride((3, 1), terrain)
}

fn task2(terrain: &Vec<String>) -> i64 {
	let slopes: [(usize, usize); 5] = [(1,1), (3,1), (5,1), (7,1), (1,2)];
	slopes.iter()
		.map(|x| ride(*x, terrain))
		.fold(1, |acc, item| acc * item)
}

fn ride(slope: (usize, usize), terrain: &Vec<String>) -> i64 {
	terrain.iter()
		.step_by(slope.1)
		.fold((0, 0), |acc, item| match {item.as_bytes()[acc.0] as char} {
			'#'  => ((acc.0 + slope.0) % 31 , acc.1 + 1),
			_ => ((acc.0 + slope.0) % 31, acc.1),

		}).1
}