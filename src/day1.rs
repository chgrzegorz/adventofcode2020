use std::fs;

pub fn solve() {
	println!("Day 1, task 1: {:?}", task1(task_data()));
	println!("Day 1, task 2: {:?}", task2(task_data()));
}

fn task1(expenses: Vec<i32>) -> i32 {
	let mut result: i32 = 0;
	for x in expenses.iter() {
		for y in expenses.iter() {
			if x + y == 2020 {
				result = x * y;
				break;
			}
		}
	}
	result
}

fn task2(expenses: Vec<i32>) -> i32 {
	let mut result: i32 = 0;
	for x in expenses.iter() {
		for y in expenses.iter() {
			for z in expenses.iter() {
				if x + y + z == 2020 {
					result = x * y * z;
					break;
				}
			}
		}
	}
	result
}

fn task_data() -> Vec<i32> {
	fs::read_to_string("data/day1.txt")
        .expect("file not found!")
        .lines()
        .map(|x| x.parse::<i32>().expect("Invalid line!"))
        .collect()
}

