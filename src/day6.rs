use std::fs;
use std::collections::HashSet;

pub fn solve() {
	let data: Vec<String> = task_data();
	println!("Day 6, task 1: {:?}", task1(&data));
	println!("Day 6, task 2: {:?}", task2(&data));
}

fn task_data() -> Vec<String> {
	fs::read_to_string("data/day6.txt")
        .expect("file not found!")
        .split("\n\n")
        .map(String::from)
        .collect()
}

fn task1(groups: &Vec<String>) -> usize {
	groups.iter()
		.map(parse_group1)
		.map(|hs| hs.len())
		.sum()
}

fn task2(groups: &Vec<String>) -> usize {
	groups.iter()
		.map(parse_group2)
		.map(|hs| hs.len())
		.sum()
}

fn parse_group1(group: &String) -> HashSet<char> {
	group.replace("\n", "")
		.chars()
		.collect::<HashSet<char>>()
}

fn parse_group2(group: &String) -> HashSet<char> {
	let all = "abcdefghijklmnopqrstuvwxyz".chars().collect::<HashSet<char>>();
	group.lines()
		.map(|x| x.chars().collect::<HashSet<char>>())
		.fold(all, |acc, item| acc.intersection(&item).cloned().collect())
}