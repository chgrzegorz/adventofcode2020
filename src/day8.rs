use std::collections::HashSet;
use std::fs;

pub fn solve() {
    let data = task_data();
    println!("Day 8, task 1: {:?}", task1(&data));
    println!("Day 8, task 2: {:?}", task2(&data));
}

fn task_data() -> Vec<(String, isize)> {
    fs::read_to_string("data/day8.txt")
        .expect("file not found!")
        .lines()
        .map(parse_instruction)
        .collect::<Vec<(String, isize)>>()
}

fn task1(code: &Vec<(String, isize)>) -> isize {
    let mut evaluated: HashSet<isize> = HashSet::new();
    find_loop(0, 0, code, &mut evaluated)
}
fn task2(code: &Vec<(String, isize)>) -> isize {
    let mut to_check: HashSet<isize> = HashSet::new();
    eval(0, 0, code, &mut to_check);
    let mut evaluated: HashSet<isize> = HashSet::new();

    for index in to_check.iter() {
        match swap(code, *index) {
            Some(new_code) => match eval(0, 0, &new_code, &mut evaluated) {
                Ok(acc) => return acc,
                Err(_) => (),
            },
            None => (),
        }
        evaluated.clear();
    }
    -1
}

fn find_loop(
    acc: isize,
    index: isize,
    code: &Vec<(String, isize)>,
    evaluated: &mut HashSet<isize>,
) -> isize {
    if !evaluated.insert(index) {
        return acc;
    }
    let instruction: &(String, isize) = &code[index as usize];
    match (instruction.0.as_str(), instruction.1) {
        ("acc", x) => find_loop(acc + x, index + 1, code, evaluated),
        ("jmp", x) => find_loop(acc, index + x, code, evaluated),
        _ => find_loop(acc, index + 1, code, evaluated),
    }
}

fn parse_instruction(instruction: &str) -> (String, isize) {
    let mut it = instruction.split(" ");
    (
        String::from(it.next().unwrap()),
        it.next().unwrap().parse::<isize>().unwrap(),
    )
}

fn eval(
    acc: isize,
    index: isize,
    code: &Vec<(String, isize)>,
    evaluated: &mut HashSet<isize>,
) -> Result<isize, isize> {
    if !evaluated.insert(index) {
        return Err(acc);
    } else if index == (code.len() - 1) as isize {
        return Ok(acc);
    }
    let instruction: &(String, isize) = &code[index as usize];
    match (instruction.0.as_str(), instruction.1) {
        ("acc", x) => eval(acc + x, index + 1, code, evaluated),
        ("jmp", x) => eval(acc, index + x, code, evaluated),
        _ => eval(acc, index + 1, code, evaluated),
    }
}

fn swap(code: &Vec<(String, isize)>, i: isize) -> Option<Vec<(String, isize)>> {
    let index: usize = i as usize;
    let instruction: &(String, isize) = &code[index];
    let new_cmd: String = match instruction.0.as_str() {
        "jmp" => String::from("nop"),
        "nop" => String::from("jmp"),
        _ => return None,
    };
    let mut new_code = code.clone();
    new_code.remove(index);
    new_code.insert(index, (new_cmd, instruction.1));
    Some(new_code)
}
